Jacamar CI v0.8.0
=================

* *Release*: `v0.8.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.8.0>`_
* *Date*: 07/28/2021

.. important::

    With this release we no longer require the usage of a patched runner.
    Instead you will need to use the
    `official release <https://docs.gitlab.com/runner/install/>`_, version
    `14.1+` for Jacamar CI to function correctly. Updating Jacamar CI will
    necessitate an update of the runner in conjunction to continue support.

Admin Changes
-------------

* Function with official runner release (`14.1+`) via the
  ``JOB_RESPONSE_FILE`` providing fully payload details
  (`!211 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/211>`_).

  - With the acceptance of `MR 2912 <https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2912>`_
    we will no longer require or maintain any patches to runner in
    order to use Jacamar CI as a custom executor.

* Added configurable allowlist for CI pipeline sources, only valid with
  server versions `14.0+`
  (`!208 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/208>`_).

  - .. code-block:: toml

        [auth]
        pipeline_source_allowlist = ["push", "web"]

  - `Possible values <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_:
    ``push``, ``web``, ``schedule``, ``api``, ``external``, ``chat``,
    ``webide``, ``merge_request_event``, ``external_pull_request_event``,
    ``parent_pipeline``, ``trigger``, or ``pipeline``. This list will change
    over as upstream GitLab server is modified.

* Optional RunAs environment configuration supplied to validation script
  (`!209 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/209>`_).

  - .. code-block:: toml

        [auth.runas]
        validation_env = ["HELLO=WORLD"]

*  Configurable job messaging during `prepare_exec` stage
   (`!216 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/216>`_).

  - .. code-block:: toml

        [general]
        job_message = """
        ****************************************************************************
                              NOTICE TO USERS

        This is an example message ....
        ****************************************************************************
        """

* Align several logging keys in Jacamar CI with those found in the runner
  (`!218 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/218>`_).

  - From now on ``job`` (previously ``jobID``), ``runner`` (previously
    ``runner-short``), and ``stage`` (previously ``ci-stage``) will match
    terminology used in the runner's system logging.

* Check for capabilities and permissions on the ``jacamar-auth`` binary
  that would be present issues if found in test environments
  (`!229 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/229>`_).

  - These changes requires that if
    `capabilities <https://linux.die.net/man/7/capabilities>`_ are found
    that the binary is protected with ``700`` permissions and no
    inheritable set are defined.

  - With errors `unobfuscated <../../admin/jacamar/troubleshoot.html#obfuscated-error-messages>`_:

    .. code-block:: console

        Running with gitlab-runner 14.1.0 (8925d9a0)
        on Jacamar CI Cap Testing MHtBUsfB
        Preparing the "custom" executor 00:09
        Error encountered during job: binary capabilities detected, ensure all group/world file permissions removed from Jacamar-Auth
        WARNING: Cleanup script failed: exit status 2
        ERROR: Preparation failed: exit status 2

* Error printed if ``jacamar-auth`` launched using invalid runner version
  (`!225 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/225>`_).

  - ``/opt/jacamar/bin`` is now `755` and we rely on administrators to manage
    both ownership/permissions if required for the standard RPM deployment.

* Allow setgid bits found in directory permissions
  (`!223 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/223>`_).

  - In previous deployments ``jacamar`` would fail in the base
    directory found was ``drwx-S----``.

* Relaxed RPM permissions and new runner capabilities RPM for requesting deployments
  (`!221 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/221>`_).

* Minimally re-packaged GitLab-Runner RPM with single binary
  (`!234 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/234>`_).

User Changes
------------

* Added prepare message to self-document key elements of the configuration
  that an affect CI jobs
  (`!226 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/226>`_).

Bug & Development Fixes
-----------------------

* Added context to ``clean_exec`` errors to hint at likely cause of issues
  (`!217 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/217>`_).
* Added mutex locking in appropriate command packages
  (`!210 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/210>`_).
* New error logging messages added and removed config file debug
  (`!220 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/220>`_).
* Validation used in JWT results (after signature/checksum) now uses ``number``
  as opposed to ``numeric``
  (`!212 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/212>`_).
* Further simplified ``Authorized`` interface for the `authuser` package
  (`!199 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/199>`_).
* Update to Go version 1.16.6
  (`!227 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/227>`_).
* Improve RPM release process for 14.1+ runners
  (`!221 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/221>`_).
