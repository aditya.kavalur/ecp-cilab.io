Jacamar CI v0.7.0
=================

* *Release*: `v0.7.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.7.0>`_
* *Date*: 05/26/2021

Admin Changes
-------------

* Support for variable ``data_dir`` expanded in downscoped user's
  environment
  (`!168 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/168>`_).

  - .. code-block:: toml

        [general]
            executor = "shell"
            data_dir = "$GPFS_USERDIR/.ci"
            variable_data_dir = true

  - By both defining a potential variable (outside of ``$HOME``) for
    the proposed ``data_dir`` and enabling with ``variable_data_dir``
    the resolved variable will be identified by the downscoped user.
    Subsequently the fully expanded ``data_dir`` will be used in the
    complete path for that CI jobs builds/cache directories.

  - **Important**: The variable will be resolved by the user but
    they are still subject to their file permissions. The potentially
    privileged ``jacamar-auth`` application will not use these values
    to take direct actions.

* ``jacamar-auth`` ProcessID captured during logging
  (`!173 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/173>`_).

* Jacamar CI RPM is now relocatable
  (`!184 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/184>`_).

  - .. code-block:: shell

        $ rpm -qip jacamar-ci-0.7.0.el7.x86_64.rpm
            Name        : jacamar-ci
            Version     : 0.7.0
            ...
            Relocations : /usr /opt

  - When installing the RPM use ``--relocate``, this is only supported
    for Jacamar CI and not the patched runner.

Bug & Development Fixes
-----------------------

* Added Jacamar CI logo
  (`!179 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/179>`_).
* Interactions with GitLab's JWKS endpoint
  (`JSON Web Key <https://datatracker.ietf.org/doc/html/rfc7517>`_)
  now correctly handled directly by the ``gitlabjwt`` package
  (`!172 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/172>`_).
* Added support for runner release *13.12*
  (`!180 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/180>`_).
* Extend seccomp functionality to support testing a block-all by default model
  (`!175 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/175>`_).

  - This model is currently only recommended for development
    purposes as we have no documented minimal set of required
    calls. Future iterations will attempt to address this.

  - .. code-block:: toml

        [auth.seccomp]
            block_all = true
            allow_calls = ["read", "write", ...]

* Basic high-level `panic recovery <https://blog.golang.org/defer-panic-and-recover>`_
  (`!171 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/171>`_).
* Support quick Go test of specific packages within Docker
  (`!176 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/176>`_).

  - ``make test-docker PACKAGE=pkg/rules``

* Extended CI build artifact duration to 1 day
  (`!181 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/181>`_).
* Updated runner generated test data scripts
  (`!182 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/182>`_).
