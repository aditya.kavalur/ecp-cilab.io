Jacamar CI v0.7.3
=================

* *Release*: `v0.7.3 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.7.3>`_
* *Date*: 06/30/2021

Admin Changes
-------------

* Remove unnecessary preparation checks causing failed jobs during the
  ``prepare_exec`` for scheduler with strict environment requirements
  (`!204 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/204>`_).

Bug & Development Fixes
-----------------------

* Updates to correct ``Makefile`` builds and improve readability with
  ``make help``
  (`!198 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/198>`_).
