Jacamar CI v0.3.2
=================

* *Release*: `v0.3.2 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.3.2>`_
* *Date*: 12/11/2020

Bug Fixes
---------

* Correctly monitor and wait on background child processes
  to complete that have been initiated by a user's script
  (`!59 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/59>`_).
* Use Go's ``net/url`` package when identifying target host for
  broker interactions.
