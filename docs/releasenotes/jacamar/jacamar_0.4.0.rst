Jacamar CI v0.4.0
=================

.. _configuration: https://ecp-ci.gitlab.io/docs/admin/jacamar/configuration.html

* *Release*: `v0.4.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.4.0>`_
* *Date*: 01/25/2021

General Notes
-------------

* Migrated two internal packages to now support a public API for key
  functionality used across multiple EPC CI projects
  (`!58 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/58>`_):

  - ``pkg/gitlabjwt``: Supported workflow for validating both the signature
    and key claims found in the CI_JOB_JWT. This JWT is validated against
    your GitLab instances JWKS endpoint (e.g., https://gitlab.com/-/jwks).
  - ``pkg/rules``: Validation for shared variables relating to either Jacamar
    specifically or the ECP CI effort as a whole. All checks that have been
    defined realize the validator.

* Addresses potential case in which failed Cobalt/Slurm jobs did
  not observe a defined NFS timeout/delay
  (`!81 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/81>`_).

  - This could only have occurred when
    `artifacts <https://docs.gitlab.com/ee/ci/yaml/#artifacts>`_
    are defined to occur ``on_failure``.

Admin Notes
-----------

* The cleanup stage (``clean`` subcommand) will now require the configuration_
  file be provided with the ``--configuration`` argument
  (`!71 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71>`_).

  - This change is important to aligning with the custom executor's
    `implementation <https://docs.gitlab.com/runner/executors/custom.html#cleanup>`_
    of logging  in the ``cleanup_exec`` stage.

  - Previously deployed instances of Jacamar-Auth did not require a second
    ``--configuration`` argument and will need manually updated to align
    with new requirement.

* Integrated all Federation information/context into the RunAs validation
  scripts, removing Federation only scripting requirements
  (`!65 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/65>`_,
  `!86 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/86>`_).

  - See the updated
    `federated documentation <https://ecp-ci.gitlab.io/docs/admin/jacamar/auth.html#federated>`_
    for details.

  - This is a potentially **important** change for deployments that are
    either leveraging Federation functionality. As it alters
    and, to a greater degree, simplifies the
    `authorization flow <https://ecp-ci.gitlab.io/docs/admin/jacamar/auth.html#authorization-flow>`_.

* Pre-RunAs list (allow/block) validation can now optionally be enabled via the
  configuration_ (`!68 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/68>`_).

  - .. code-block:: toml

        [auth]
          lists_pre_validation = true
          ...

  - In previous iterations this ran regardless of configuration immediately prior
    to RunAs validation. By integration Federation validation into RunAs
    maintaining the separate lists is no longer required.

* Jacamar subcommand (``jacamar translate``) added to "translate" a runner
  configuration from the forked deployment to a currently observed
  Jacamar configuration_
  (`!63 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/63>`_).

  - .. code-block:: shell

          $ jacamar translate --help
            Usage: jacamar translate SOURCE TARGET

            Positional arguments:
            SOURCE                 The GitLab configuration TOML you wish to translate (default: /etc/gitlab-runner/config.toml).
            TARGET                 Optional target file for the newly generated Jacamar configuration file, default to current working directory when not provided.

            Options:
              --help, -h             display this help and exit
              --version              display version and exit

* Added experimental `plugin <https://golang.org/pkg/plugin/>`_
  support for RunAs validation
  (`!76 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/76>`_).

  - To avoid Go issues shared context between Jacamar and an admin defined
    plugin is established via the
    `Jacamar Plugins project <https://gitlab.com/ecp-ci/jacamar-plugins>`_.

* ``jacamar--auth`` now supports an ``--unobfuscated`` flag that
  allows all errors to appear regardless of stage.
  (`!83 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/83>`_)

  - .. code-block:: toml

        [runners.custom]
          config_args = ["--unobfuscated", "config", ...]
          ...

  - All errors relating to authorization, job context identification,
    user downscoping, and JWT validation are replaced in the user
    job log with generic error messages by default.

* Update supported GitLab-Runner to version ``13.7.0``, with minimized
  patching requirements
  (`!72 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/72>`_).

* Added configurable timeout for Jacamar-Auth to wait before sending ``SIGKILL``
  (`!71 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71>`_).

* Updated OLCF focused testing structure for Ascent
  (`!62 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/62>`_).

Bug & Development Fixes
-----------------------

* Updated to `Go release 1.15.6 <https://golang.org/doc/devel/release.html#go1.15>`_
  (`!64 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/64>`_).
* Correctly establish default values for configuration_ in all cases
  (`!71 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71>`_).
* Cleaner error handling within ``jacamar-auth`` to enforce default obfuscation
  for any potentially sensitive error
  (`!74 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/74>`_).
* Correctly leverage ``_prefix`` macro in RPM spec
  (`!79 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/79>`_).
* Added Go/GCC to Pavilion containers and updated associated test
  scripts to build Jacamar requirements
  (`!78 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/78>`_,
  `!80 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/80>`_).
* System logging of ``jacamar-auth`` now handled through the Logrus
  packages (`!82 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/82>`_).
* Improve RPM related make commands
  (`!85 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/85>`_).
