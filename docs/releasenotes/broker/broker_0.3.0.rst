CI Token Broker v0.3.0
=======================

* *Release*: `v0.3.0 <https://gitlab.com/ecp-ci/ci-token-broker/-/releases/v0.3.0>`_
* *Date*: 5/24/2021

.. Warning::

    The CI Token Broker is not meant for production systems at this time and
    should only be used in development/test environment to inspect the
    validity of the workflow.

Admin Changes
-------------

* Service account registered validated before accepted
  (`!28 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/28>`_).

Bug & Development Fixes
-----------------------

* Delete ``X-Forward-Host`` per Go documentation
  (`!27 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/27>`_).
* Updated to ``docker-compose`` enabled development environment
  (`!26 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/26>`_,
  `!30 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/30>`_).

  - User ``make dev-docker`` to get started, see the
    ``build/dev-env/README.md`` for complete details.

* Update Jacamar CI package dependencies
  (`!29 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/29>`_)
