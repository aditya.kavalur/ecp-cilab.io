Administration
==============

.. _introduction: introduction.html#enhancements
.. _official documentation: https://docs.gitlab.com

Documentation to support administrating GitLab runners and servers targeting
HPC environments. The information provided here is meant to supplement the
`official documentation`_ in areas where it may
not address HPC focused questions while also providing comprehensive details
on all ECP CI enhancements. For details on using the runners themselves
please see the `CI users <ci-users.html>`_ documentation.

Jacamar CI
----------

Jacamar is the HPC focused CI/CD driver for GitLab's
`custom executor <https://docs.gitlab.com/runner/executors/custom.html>`_.
The core goal of this project is to establish a maintainable,
yet extensively configurable tool that will allow for the use of GitLab’s
robust CI model on unique HPC test resources. Allowing code teams to
integrate potentially existing pipelines on powerful scientific development
environments.

.. toctree::
   :maxdepth: 1

   admin/jacamar/introduction.rst
   admin/jacamar/tutorial.rst
   admin/jacamar/deployment.rst
   admin/jacamar/configuration.rst
   admin/jacamar/auth.rst
   admin/jacamar/executors.rst
   admin/jacamar/troubleshoot.rst

Though the `open-source project <https://gitlab.com/ecp-ci/jacamar-ci>`_ is
still being developed, it has reached a stage where we want to encourage early
adoption. Any feedback or identifiable requirements are appreciated.

CI Token Broker
---------------

.. important::

    With upstream server changes in *14.1+*, it is now possible limit the scope
    of CI Job Tokens at the project level (see
    `related documentation <https://docs.gitlab.com/ee/api/index.html#limit-gitlab-cicd-job-token-access>`_
    ). As such we've decided that the token broker will no longer be pursued
    and support removed from Jacamar CI. Thank you for feedback and interest
    in this functionality.

Server
------

`GitLab <https://about.gitlab.com/what-is-gitlab/>`_ offers a
comprehensive development platform all within a single application. A major
benefit to using this tool is its both predominately
`open source <https://gitlab.com/gitlab-org/gitlab>`_ and allows for
management of self-hosted instances. However, there are a
`number of features <https://about.gitlab.com/pricing/self-managed/feature-comparison/>`_
that necessitate the purchase of a license.

All documentation organized here is not written to replace any exiting
`official documentation`_ offered. We only seek to highlight
specifics that are directly related to HPC centers. We fully advise using
GitLab's documentation wherever possible as it provides up-to-date information.

.. toctree::
   :maxdepth: 2

   admin/server-admin.rst

Cross-Site CI (Federation)
~~~~~~~~~~~~~~~~~~~~~~~~~~

A goal of the ECP CI effort is to enable long-term support for cross-site
continuous integration. With this we are targeting expanding GitLab's current
authentication model to allow the management of multiple providers to
expand beyond the login process. Fundamentally a user's valid authentication
provider will become a central part of the CI job process.

Related enhancements are currently under active development. Future changes to
this model can/will occur without notification. As such any
documentation should be considered only useful for enabling collaborative
testing of related enhancements and not suitable for production deployments.

.. toctree::
   :maxdepth: 2

   admin/federation.rst

Auditing
~~~~~~~~

Auditing a user's interactions with a GitLab deployment is an important
administrative component. Although the runner may be completely open-source
there are aspects of the server, including
`Audit Event <https://docs.gitlab.com/ee/administration/audit_events.html>`_,
that require a paid subscription. To understanding logging and what can
be accomplished with your given license please see the official
`log system <https://docs.gitlab.com/ee/administration/logs.html>`_
administrator documentation.

Guides
------

Guides have been written to highlight both best practices as well as
potential workflows you may want to leverage when supporting ECP
CI on facility test resources.

.. list-table::
    :header-rows: 1
    :widths: 15, 7, 7, 25

    * - Title
      - Jacamar CI
      - Server
      - Description
    * - `Non-Root Jacamar CI Downscoping (with Capabilities) via SetUID <guides/non-root-deployment-setuid.html>`_
      - √
      - x
      - How to deploy, configure, and run Jacamar CI with *setuid* downscoping as a non-root user.
    * - `Non-Root Jacamar CI Downscoping via Sudo <guides/non-root-deployment-sudo.html>`_
      - √
      - x
      - How to deploy, configure, and run Jacamar CI with *sudo* downscoping as a non-root user.
    * - `Tracing CI Jobs with Administrative Tools <guides/admin-job-tracing.html>`_
      - √
      - x
      - Using recommended configurations for Jacamar CI along with server logs to proper trace job execution.

.. toctree::
   :maxdepth: 1
   :hidden:

   guides/non-root-deployment-setuid.rst
   guides/non-root-deployment-sudo.rst
   guides/admin-job-tracing.rst

Latest Releases
---------------

Detailed notes for all software releases can be found
`here <releasenotes/all.html>`_.

.. toctree::
   :maxdepth: 1

   releasenotes/jacamar/jacamar_0.8.1.rst
   releasenotes/jacamar/jacamar_0.8.0.rst
