#define _GNU_SOURCE

#include <utmpx.h>
#include <stdio.h>
#include <omp.h>
#include <unistd.h>
#include <stdlib.h>
#include "mpi.h"

int main (int argc, char *argv[]) {
  int           my_rank=0;
  int           sz;
  int sched_getcpu();
  int cpu;
  int thread_id;
  char hostname[MPI_MAX_PROCESSOR_NAME]="UNKNOWN";

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Get_processor_name(hostname, &sz);

  #pragma omp parallel private(cpu,thread_id)
  {
     thread_id = omp_get_thread_num();
     cpu = sched_getcpu();
     printf("Hello, from host %s from CPU %d  rank %d thread %d\n", hostname, cpu, my_rank, thread_id);
     if (argc > 1)
        sleep (atoi(argv[1]));
  }

  MPI_Finalize();
  
  return 0;
}
